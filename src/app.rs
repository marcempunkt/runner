use std::env;
use std::fs;
use std::process::Command;
use std::os::unix::process::CommandExt;
use core::ops::Range;
use std::fs::{ReadDir, DirEntry};
use eframe::{Frame, CreationContext};
use egui::{Context, Ui};

pub struct App {
    search: String,
    executables: Vec<Executable>,
}

#[derive(Clone)]
struct Executable {
    app_name: String,
    app_full_path: String,
    app_type: AppType,
}

#[derive(Clone)]
enum AppType {
    Traditional,
    Flatpak,
    Snap,
    Cargo,
    Nix,
    Roswell,
}

impl AppType {
    pub fn new(app_full_path: impl Into<String>) -> Self {
        let app_full_path: String = app_full_path.into();
        if app_full_path.contains("flatpak") {
            Self::Flatpak
        } else if app_full_path.contains("snap") {
            Self::Snap
        } else if app_full_path.contains("cargo") {
            Self::Cargo
        } else if app_full_path.contains("nix") {
            Self::Nix
        } else if app_full_path.contains("roswell") {
            Self::Roswell
        } else {
            Self::Traditional
        }
    }

    pub fn to_str(&self) -> &str {
        match self {
            &Self::Flatpak => " (flatpak)",
            &Self::Snap => " (snap)",
            &Self::Cargo => " (cargo)",
            &Self::Nix => " (nix)",
            &Self::Roswell => " (roswell)",
            _ => "",
        }
    }
}

impl App {
    pub fn new(cc: &CreationContext) -> Self {

        let path: String = env::var("PATH").unwrap();
        let path: Vec<&str> = path.split(":").collect();
        let mut executables: Vec<Executable> = vec![];

        path.iter().for_each(|path: &&str| {
            let dir: Result<ReadDir, std::io::Error> = fs::read_dir(path);
            if let Ok(dir) = dir {
                dir.into_iter()
                    .for_each(|dir: Result<DirEntry, std::io::Error>| {
                        if let Ok(dir) = dir {
                            let app_name: String = dir.file_name().into_string().unwrap();
                            let app_full_path: String = dir.path().to_str().unwrap().to_string();
                            let app_type: AppType = AppType::new(&app_full_path); 
                            executables.push(Executable { app_name, app_full_path, app_type });
                        }
                    });
            }
        });

        Self {
            search: String::new(),
            executables,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &Context, frame: &mut Frame) {
        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            ui.text_edit_singleline(&mut self.search);

            let executables: Vec<Executable> = self.executables
                .clone()
                .into_iter()
                .filter(|exe: &Executable| {
                    let search_splitted: Vec<&str> = self.search.split(" ").collect();
                    return search_splitted
                        .into_iter()
                        .all(|search: &str| exe
                             .app_full_path
                             .to_lowercase()
                             .contains(search));
                })
                .collect();

            egui::ScrollArea::vertical().show_rows(ui, 10., executables.len(), |ui: &mut Ui, row_range: Range<usize>| {
                row_range.for_each(|row: usize| {
                    let exe: &Executable = &executables.get(row).unwrap();
                    if ui.button(format!("{}{}", &exe.app_name, &exe.app_type.to_str())).clicked() {
                        Command::new(&exe.app_full_path)
                            .exec();
                    }
                });
            });
        });
    }
}


pub mod app;
use app::App;

fn main() {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "runner",
        native_options,
        Box::new(|cc| Box::new(App::new(cc))),
    ).unwrap();
}
